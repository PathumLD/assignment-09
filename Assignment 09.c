/*C program which writes a sentence to a file, reads
and appending another line to file.*/

#include<stdio.h>

int main(){


    char *fn = "assignment9.txt";

    FILE *fptr = fopen(fn, "w" );

    fprintf(fptr, "UCSC is one of the leading institutes in Sri Lanka for computing studies.\n");

    fclose(fptr);


    fptr = fopen(fn,"r");

    char ch;
    while ((ch = fgetc(fptr)) != EOF)
        putchar(ch);

    fclose(fptr);


    fptr = fopen(fn, "a");

    fprintf(fptr, "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.\n");

    fclose(fptr);
}
